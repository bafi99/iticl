<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iticl');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h%w1hu?-z<n6*<V7&&nmt1#Kc#k8zC<=[e5Cx&=>7`{bI*;}7F_d@y8%=BG:l=a7');
define('SECURE_AUTH_KEY',  '7pRA(dP Y@6Q!uKZUVk<>k$cd@L>l%nY#N]xxrPW?l^H&}_&?G(oX%9#nPEOrM{J');
define('LOGGED_IN_KEY',    'Hh_uU&J+$Vkpm%bM:,g:SZw9NZ(C;eBH?z2F~))Kb/3OfTgr#>?*<;}1:!6`Wby>');
define('NONCE_KEY',        'Z(<j^iQF?dUm:mJ/ZFaJXQv>_P>_,(9`Sq8c6Czfyo3[S2 nQ<u3)HYq)[`_Gd1t');
define('AUTH_SALT',        '<vbDtm8%h<b<H{w)EV)5Sh^_Ru:?3%75^[St&TfBlBA>nvxcFww=)vauQ9Y P[k/');
define('SECURE_AUTH_SALT', '4wggC[BhvaJtMy~T_2,]G*sV6FvA[vd}V;O>4w*_(ew?9%U4brSPw[rbH_,O#KDw');
define('LOGGED_IN_SALT',   'G;>RG}v;HTn3t:gylKe9r.nJV(jIKv>bL^tGAnt,K Z=4H|>8hB]A2Gt]}ye77IM');
define('NONCE_SALT',       'UrLHS1!~cz{V]}5ro0]q2x|7Mwi2nfTonsB;*8(-h99b<M95`=nM$W ~/vs#Z,9g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'it_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
